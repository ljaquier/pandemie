/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package app;

/**
 * Une interface pour decrire les objets qui peuvent etre identifie 
 * de maniere unique (en utilisant IDGenerator)
 * @see IDGenerator
 */
public interface Identifiable {
  /**
   * Return l'id identifiant cet objet
   * @return l'id de l'objet
   */
  public int getID ();
}
