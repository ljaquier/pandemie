/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package app;

/**
 * Singleton qui génère des identificateurs uniques sous la forme d'integer
 */
public class IDGenerator {
  private static IDGenerator instance;
  
  private IDGenerator () {}
  
  /**
   * Accède à l'instance 
   * @return l'instance de IDGenerator à utiliser
   */
  public static IDGenerator getInstance() {
    if (instance == null)
      instance = new IDGenerator();
    return instance;
  }
  
  private int nextID = 0;
  
  /**
   * Créer une ID unique
   * @return une nouvelle ID unique
   */
  public int createID() {
    return nextID++;
  }
  
  
}
