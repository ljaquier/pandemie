/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Cette classe offre des fonctionnalités de journal via sa méthode log
 * Elle peut être observé et notifie ses observateurs lorsqu'une nouvelle ligne
 * est ajoutée au journal.
 */
public class Logger extends Observable {
  private static Logger instance;
  
  private List<String> lines = new ArrayList<String>();
  
  private Logger () {}
  
  /**
   * Permet d'accéder à l'instance de logger
   * @return l'instance de Logger à utiliser
   */
  public static Logger getInstance () {
    if (instance == null)
      instance = new Logger();
    return instance;
  }
  
  /**
   * Retourne l'integralité du journal, ligne par ligne
   * @return les lignes constituant le journal
   */
  public List<String> getLines () {
    return lines;
  }
  
  /**
   * Ajoute une nouvelle ligne au journal
   * @param s la ligne à ajouter
   */
  public void log (String s) {
    lines.add(s);
    
    this.setChanged();
    this.notifyObservers(s);
  }
  
  /**
   * Ajoute une nouvelle ligne au journal avec identification de l'appelant
   * @param caller l'objet (identifiable) ajoutant la ligne
   * @param s la ligne à ajouter
   */
  public void log (Identifiable caller, String s) {
    //Si le log vient d'un objet identifiable, on affiche son id
    lines.add("["+caller.getClass().getName()+"@"+((Identifiable)caller).getID()+"] " + s);

    this.setChanged();
    this.notifyObservers(s);
  }
  
  /**
   * Ajoute une nouvelle ligne au journal
   * @param caller l'objet ayant appelé le journal
   * @param s la ligne à ajouter
   */
  public void log (Object caller, String s) {
    lines.add("["+caller.getClass().getName()+"] " + s);
    
    this.setChanged();
    this.notifyObservers(s);
  }
}
