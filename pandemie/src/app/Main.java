/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package app;


/**
 * Classe principale de l'application
 */
public class Main {
	public static void main (String argv[]) {
		new gui.WorldView(new World(5));
	}
}
