/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package visitable;

import java.util.Observable;

import visitors.Visitor;

/**
 * Modelise une connexion entre deux creatures par laquelle un visiteur peut se propager
 * 
 * Une connection est observable.
 * Lorsque le visiteur commence à passer par cette connection, elle va notifier ses observeurs 
 * avec le visiteur comme argument
 * Lorsque le visiteur a terminé de passer par cette connection, elle va notifier ses observeurs
 * avec null comme argument
 * 
 */
public class CreatureConnection extends Observable {
  private Creature c1, c2;
  
  /**
   * Une CreatureConnection ne doit être crée que part ConnectionManager
   * @see ConnectionManager.addConnection
   */
  protected CreatureConnection (Creature c1, Creature c2) {
    this.c1 = c1;
    this.c2 = c2;
  }
  
  //Fais passer un visiteur a travers cette connexion
  public void spread (Creature from, Visitor v) {
    //On regarde de quel côté vient le visiteur
    Creature to = null;
    if (from == c1)
      to = c2;
    else if (from == c2)
      to = c1;
    else
      throw new IllegalArgumentException("from is not part of this CreatureConnection");
    
    boolean accepted = to.accept(v);
    
    this.setChanged();
    this.notifyObservers(v);
    //Prends du temps pour passer la connexion uniquement si le visiteur est accepté
    if(accepted) { 
       try {
         Thread.sleep(1000);
       } catch (InterruptedException e) {}
    }
    
    this.setChanged();
    this.notifyObservers(null);
  }
  
  //Obtient la première créature connecté
  public Creature getFirstCreature () {
    return c1;
  }
  
  //Obtient la seconde créature connectée
  public Creature getSecondCreature () {
    return c2;
  }
  
  /** 
   * Deux connexions sont considérés égales si elles connectent les mêmes créatures,
   * peu importe l'ordre
   */
  @Override
  public boolean equals (Object other) {
    if (other.getClass() == this.getClass()) {
      CreatureConnection conn = (CreatureConnection)other;
      return (conn.c1 == c1 && conn.c2 == c2) ||
             (conn.c1 == c2 && conn.c2 == c1);
    } else {
      return super.equals(other);
    }
  }
}
