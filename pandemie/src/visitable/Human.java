/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package visitable;

import visitors.Visitor;

/**
 * Un humain
 */
public class Human extends Creature {
	@Override
	protected void _accept(Visitor visitor) {
	  visitor.visit(this);
	}
}
