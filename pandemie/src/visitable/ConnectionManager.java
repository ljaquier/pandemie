/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package visitable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * On utilise un gestionnaire de connexions pour maintenir une liste centrale des connexions entres les créatures.
 * Ce gestionnaire est un singleton
 */
public class ConnectionManager {
  private static ConnectionManager instance;
  private ConnectionManager () {}
  /**
   * Accès à l'instance
   */
  public static ConnectionManager getInstance () {
    if (instance == null)
      instance = new ConnectionManager();
    return instance;
  }
  
  //On utilise un hashset si on veut un access unique a chacune des connections
  //Une connection est donc stocke a la fois dans connections et dans connectionSet
  private HashSet<CreatureConnection> connectionsSet = new HashSet<CreatureConnection>();
  //Pour chaque creature, stock la liste de ses connexions
  private HashMap<Creature, HashSet<CreatureConnection>> connections = new HashMap<Creature, HashSet<CreatureConnection>>();
  
  /**
   * Ajoute une connexion pour une creature. 
   * Si c'est la premiere connexion pour cette creature, cree la List<CreatureConnection> de connexions de la crature 
   */
  private void addConnectionForCreature (Creature c, CreatureConnection conn) {
    if (!connections.containsKey(c))
      connections.put(c, new HashSet<CreatureConnection>());
    Set<CreatureConnection> set = connections.get(c);
    if (set.contains(conn)) {
      //Si la connexion est deja dedans, c'est qu'on cree une double connexion bidirectionnelle, donc
      //on leve une exception
      throw new IllegalArgumentException("Connexion deja existante");
    }
    set.add(conn);
  }
  
  /**
   * Supprimer toutes les connexions
   */
  public void clearConnections () {
    connectionsSet.clear();
    connections.clear();
  }
  
  /**
   * Ajoute une nouvelle connexion entre deux créatures
   */
  public void addConnection (Creature c1, Creature c2) {
    CreatureConnection conn = new CreatureConnection(c1,c2);
    addConnectionForCreature(c1, conn);
    addConnectionForCreature(c2, conn);
    connectionsSet.add(conn);
  }
  
  /**
   * Obtient la liste des connexions pour une créature donnée
   */
  public Set<CreatureConnection> getConnections (Creature c) {
    //On cree une liste vide pour eviter de renvoyer null
    if (!connections.containsKey(c))
      connections.put(c, new HashSet<CreatureConnection>());
    
    return connections.get(c);
  }
  
  /**
   * Obtient la liste de toutes les connexions
   */
  public Set<CreatureConnection> getConnectionsSet () {
    return connectionsSet;
  }
  
}
