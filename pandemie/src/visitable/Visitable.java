/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package visitable;

import visitors.Visitor;

/**
 * Les objets implémentant cette interface peuvent être visités
 */
public interface Visitable {
	public boolean accept(Visitor visitor);
}
