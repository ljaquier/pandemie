/**
 * Fichier: FowlFlu.java
 * Date   : 22 mai 2009
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package visitors;

import app.Logger;
import visitable.Chicken;
import visitable.Human;
import visitable.Pig;

/**
 * But: Représenter la grippe aviaire
 */
public class FowlFlu extends Visitor {

   private int nbHumanContaminated = 0;
   
	@Override
	public void visit(Chicken chicken) {
	   // Si le poulet est en vie
	   if(chicken.isAlive()) {
	      // On le tue
   	   chicken.die();
         Logger.getInstance().log(this,"La grippe aviaire a tué le poulet " + chicken.getID());
         // On propage la maladie
         chicken.spread(this);
	   }
	}

	@Override
  public void visit(Human human) {
    // Si l'humain est en vie
    if (human.isAlive()) {
      synchronized (this) {
        nbHumanContaminated++;
        // On ne tue que un humain sur quatre contaminé
        if (nbHumanContaminated % 4 == 0) {
          human.die();
          Logger.getInstance().log(this, "La grippe aviaire a tué l'humain " + human.getID());
        } else
          Logger.getInstance().log(this, "L'humain " + human.getID() + " est chanceux et résiste à la grippe aviaire");
      }
      // On propage la maladie
      human.spread(this);
    }
  }

	@Override
	public void visit(Pig pig) {
	  Logger.getInstance().log(this, "La grippe aviaire ne tue pas le cochon " + pig.getID());
	}
}
