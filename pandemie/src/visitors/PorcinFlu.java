/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package visitors;

import app.Logger;
import visitable.Chicken;
import visitable.Human;
import visitable.Pig;

/**
 * But: Représenter la grippe porcine
 */
public class PorcinFlu extends Visitor {

	@Override
	public void visit(Chicken chicken) {
	   Logger.getInstance().log(this, "La Grippe Porcine ne tue pas le poulet " + chicken.getID());
	}

	@Override
	public void visit(Human human) {
	   // Si l'humain est en vie
      if(human.isAlive()) {
        // On le tue
        human.die();
        Logger.getInstance().log(this,"La grippe porcine a tué l'humain " + human.getID());
        // On propage la maladie
        human.spread(this);
      }
	}

	@Override
	public void visit(Pig pig) {
	   // Si le porc est en vie
      if(pig.isAlive()) {
         // On le tue
         pig.die();
         Logger.getInstance().log(this,"La grippe porcine a tué le cochon " + pig.getID());
         // On propage la maladie
         pig.spread(this);
      }
	}

}
