/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package visitors;

import app.IDGenerator;
import app.Identifiable;
import visitable.Chicken;
import visitable.Human;
import visitable.Pig;

/**
 * But: Représenter un visiteur
 */
public abstract class Visitor implements Identifiable {
  private int id;
  
  /**
   * Constructeur
   */
  public Visitor () {
    id = IDGenerator.getInstance().createID();
  }
  
  /**
   * Return l'id identifiant cet objet
   * @return l'id de l'objet
   */
  public int getID () {
    return id;
  }
  
	/**
	 * Pour visiter un humain
	 * @param human L'humain à visiter
	 */
	public abstract void visit(Human human);
	
	
	/**
	 * Pour visiter un porc
	 * @param pig Le porc
	 */
	public abstract void visit(Pig pig);
	
	
	/**
	 * Pour visiter un poulet
	 * @param chicken Le poulet à visiter
	 */
	public abstract void visit(Chicken chicken);
	
}
