/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package visitors;

import app.Logger;
import visitable.Chicken;
import visitable.Human;
import visitable.Pig;

/**
 * But: Représenter le SRAS
 */
public class SRAS extends Visitor {

	@Override
	public void visit(Chicken chicken) {
		Logger.getInstance().log(this, "Le sras ne tue pas le poulet " + chicken.getID());
	}

	@Override
	public void visit(Human human) {
	   // Si l'humain est en vie
	   if(human.isAlive()) {
	      // On le tue
   	   human.die();
   	   Logger.getInstance().log(this,"Le sras tue l'humain " + human.getID());
         // On propage la maladie
   	   human.spread(this);
	   }
	}

	@Override
	public void visit(Pig pig) {
	  Logger.getInstance().log(this, "Le sras ne tue pas le cochon " + pig.getID());
	}

}
