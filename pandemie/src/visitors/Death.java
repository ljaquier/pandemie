/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package visitors;

import app.Logger;
import visitable.Chicken;
import visitable.Human;
import visitable.Pig;

public class Death extends Visitor {

  @Override
  public void visit(final Human human) {
     // Si l'humain est en vie
     if(human.isAlive()) {
        Logger.getInstance().log(this,"La mort a tué l'humain " + human.getID());
        // On le tue et on propage la maladie
        human.die();
        human.spread(this);
     }
  }

  @Override
  public void visit(final Pig pig) {
     // Si le cochon est en vie
     if(pig.isAlive()) {
        Logger.getInstance().log(this,"La mort a tué le cochon " + pig.getID());
        // On le tue et on propage la maladie
        pig.die();
        pig.spread(this);
     }
  }

  @Override
  public void visit(final Chicken chicken) {
     // Si le poulet est en vie
     if(chicken.isAlive()) {
        Logger.getInstance().log(this,"La mort a tué le poulet " + chicken.getID());
        // On le tue et on propage la maladie
        chicken.die();
        chicken.spread(this);
     }
  }

}
