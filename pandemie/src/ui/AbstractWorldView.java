/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package ui;

import java.util.Observable;
import java.util.Observer;

import app.Logger;
import app.World;

/**
 * Classe de base pour toutes les visualisations du monde
 */
public abstract class AbstractWorldView implements Observer {
	protected World world;

	public AbstractWorldView(World v) {
		this.world = v;
		Logger.getInstance().addObserver(this);
		/**
		 * Si on a loupé (parce qu'on observait pas encore) certains messages de
		 * logger, on les traite maintenant
		 */
		for (String s : Logger.getInstance().getLines())
			newLogLine(s);
	}

	/**
	 * Associe un nouveau monde à la vue
	 * @param v Le nouveau monde
	 */
	public void setWorld(World v) {
		world = v;
	}

	/**
	 * Méthode appelée lorsqu'une nouvelle ligne est ajoutée au Logger
	 * @param newLine la nouvelle ligne ajoutée
	 */
	protected abstract void newLogLine(String newLine);

	@Override
	public final void update(Observable o, Object arg) {
		// New log line
		if (o == Logger.getInstance()) {
			String newLine = (String) arg;
			newLogLine(newLine);
		}
	}
}
