/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package gui;

import java.io.File;

import visitable.Pig;

/**
 * Représentation graphique d'un porc
 */
public class PigView extends CreatureView {
	
	private final static String urlImageAlivePig = System.getProperty("user.dir") + File.separator + "res" + File.separator + "textures" + File.separator + "alivePig.gif";
	private final static String urlImageDeadPig = System.getProperty("user.dir") + File.separator + "res" + File.separator + "textures" + File.separator + "deadPig.gif";
	private static final long serialVersionUID = 1L;
	public PigView(Pig creature, WorldView worldView) {
		super(creature, worldView, urlImageAlivePig, urlImageDeadPig);
	}
}
