/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import visitable.Creature;
import visitors.Death;
import visitors.FowlFlu;
import visitors.PorcinFlu;
import visitors.SRAS;
import visitors.Visitor;

/**
 * Menu pour choisir quelle maladie lancer.
 * Cette classe est un singleton, il y a forcément un seul menu
 */
public class DiseaseMenu extends JPopupMenu {

  private static final long serialVersionUID = 1L;
  private static DiseaseMenu instance;
  private Creature creature;

  /**
   * Affiche le menu pour une certaine créature
   * @param c la créature sur laquelle on va lancer la maladie
   * @param x position en x (coordonnées comp) où afficher le menu
   * @param y position en y (coordonnées comp) où afficher le menu
   * @param comp
   */
  public static void showMenuFor(Creature c, int x, int y, Component comp) {
    if (instance == null)
      instance = new DiseaseMenu();
    instance.creature = c;
    instance.show(comp, x, y);
  }

  private DiseaseMenu() {
    super();
    add(createVisitorMenuItem("SRAS", SRAS.class));
    add(createVisitorMenuItem("Grippe porcine", PorcinFlu.class));
    add(createVisitorMenuItem("Grippe aviaire", FowlFlu.class));
    add(createVisitorMenuItem("Mort", Death.class));
  }

  /**
   * Cree un nouveau menuItem qui permet de lancer un visiteur sur la creature
   * pické
   * 
   * @param title le nom du menuItem
   * @param visitorClass la class de visiteur a creer (DOIT etre un sous-type de Visitor
   * @return
   */
  private JMenuItem createVisitorMenuItem(String title, final Class<? extends Visitor> visitorClass) {
    JMenuItem item = new JMenuItem(title);
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent a) {
        new Thread() {
          public void run() {
            try {
              creature.accept((Visitor) visitorClass.newInstance());
            } catch (InstantiationException e) {
              e.printStackTrace();
            } catch (IllegalAccessException e) {
              e.printStackTrace();
            }
          }
        }.start();
      }
    });
    return item;
  }
}
