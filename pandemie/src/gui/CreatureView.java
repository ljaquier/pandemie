/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package gui;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import visitable.Creature;
import app.Logger;

/**
 * Représentation graphique d'une créature
 */
public abstract class CreatureView extends JPanel implements Observer {
  //La portion maximum de la largeur/hauteur de la cellule utilisée pour afficher l'image 
  //(le reste ça sera pour les liens)
  public final static double imagePortion = 0.5;
  //Epaisseur de la bordure autour de l'image
  public final static int borderWidth = 2;
  
  private Image aliveImage;
  private Image deadImage;
  private Image currentImage;
  private Creature creature;
  private WorldView world;

  /**
   * Constructeur 
   * @param creature la créature qu'on représente
   * @param worldview la vue du monde qui contient la créature
   * @param aliveImageUrl l'image à utiliser quand la créature est vivante
   * @param deadImageUrl l'image à utiliser quand la créature est morte
   */
  public CreatureView(Creature creature, 
                      WorldView worldview,
                      String aliveImageUrl,
                      String deadImageUrl) {
    super();
    aliveImage = Toolkit.getDefaultToolkit().getImage(aliveImageUrl);
    deadImage = Toolkit.getDefaultToolkit().getImage(deadImageUrl);
    currentImage = aliveImage;
    this.creature = creature;
    this.world = worldview;
    creature.addObserver(this);

    // Callback pour les clics de souris (picking
    this.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON3) {
          Logger.getInstance().log(
              "clicked creature : " + CreatureView.this.creature.getID());
          DiseaseMenu.showMenuFor(CreatureView.this.creature, evt.getX(), evt
              .getY(), CreatureView.this);
        }
      }
    });

    // Callback pour les mouvements de souris, pour mettre en évidence les
    // objets pouvant être sélectionnés
    this.addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent evt) {
      }
    });
  }

  /**
   * Dessin de la créature
   */
  @Override
  public void paintComponent(Graphics g) {
    Graphics2D g2d = (Graphics2D)g;
    final int imageWidth = (int)(getWidth()*imagePortion);
    final int imageHeight = (int)(getHeight()*imagePortion);
    
    final int xPadding = (getWidth() - imageWidth)/2;
    final int yPadding = (getHeight() - imageHeight)/2;
    
    //top-left, bottom-right coords
    final int tlX = xPadding;
    final int tlY = yPadding;
    
    // On efface l'image précédente et on dessine la nouvelle
    g2d.clearRect(tlX, tlY, imageWidth, imageHeight);
    g2d.drawImage(currentImage, tlX, tlY, imageWidth, imageHeight, this);
    g2d.setStroke(new BasicStroke(borderWidth));
    g2d.drawRect(tlX, tlY, imageWidth, imageHeight);
  }


  @Override
  public void update(Observable o, Object arg) {
    //Quand on est notifie par la creature, c'est que cette dernier a recu la maladie.
    if (creature.isAlive())
      currentImage = aliveImage;
    else
      currentImage = deadImage;
    world.repaint();
  }
}
