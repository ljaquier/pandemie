/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */
package gui;

import java.awt.*;
import java.util.*;

import visitable.CreatureConnection;

/**
 * Représentation visuelle d'une connection par une ligne entre les deux créatures
 */
public class ConnectionView implements Observer {
  private CreatureView c1;
  private CreatureView c2;
  private Color currentColor = Color.RED;
  private WorldView world;
  
  /**
   * Constructeur
   * @param c la connexion dont cet objet est une représentation visuelle
   * @param c1 la vue de la créature correspondant à un côté de la connexion
   * @param c2 la vue de la créature correspondant à l'autre côté de la connexion
   */
  public ConnectionView (CreatureConnection c,
                         CreatureView c1,
                         CreatureView c2,
                         WorldView worldview) {
    c.addObserver(this);
    this.c1 = c1;
    this.c2 = c2;
    this.world = worldview;
  }
  
  /**
   * Dessin de la connexion
   */
  public void draw (Graphics2D g) {
    final Point loc1 = c1.getLocation();
    final Point loc2 = c2.getLocation();
    final Dimension dim1 = c1.getSize();
    
    //On sait que les deux creatureView ont la meme taille parce qu'on utilise un gridlayout
    final int imageWidth = (int)(dim1.width*CreatureView.imagePortion);
    final int imageHeight = (int)(dim1.height*CreatureView.imagePortion);
    
    final int xPadding = (dim1.width - imageWidth)/2 - 1 - CreatureView.borderWidth;
    final int yPadding = (dim1.height - imageHeight)/2 - 1 - CreatureView.borderWidth;
    
    g.setColor(currentColor);
    
    if (loc1.y == loc2.y) {
      //connexion horizontale
      g.drawLine(loc1.x + dim1.width - xPadding, loc1.y + dim1.height/2,
                 loc2.x + xPadding, loc2.y + dim1.height/2);
    } else {
      //connexion verticale
      g.drawLine(loc1.x + dim1.width/2, loc1.y + dim1.height - yPadding,
                 loc2.x + dim1.width/2, loc2.y + yPadding);
    }
  }

  /**
   * Fonction d'observation de la connexion
   */
  @Override
  public void update(Observable conn, Object v) {
    if (v != null)
      //Le visiteur commence à passer par la connexion
      currentColor = Color.GREEN;
    else
      //Le visiteur a terminé son passage par la connexion
      currentColor = Color.BLUE;
    world.repaint();
  }
}
