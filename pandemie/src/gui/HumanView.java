/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package gui;

import java.io.File;

import visitable.Human;

/**
 * Représentation graphique d'un humain
 */
public class HumanView extends CreatureView {
	private final static String urlImageAliveHuman = System.getProperty("user.dir") + File.separator + "res" + File.separator + "textures" + File.separator + "aliveHuman.gif";
	private final static String urlImageDeadHuman = System.getProperty("user.dir") + File.separator + "res" + File.separator + "textures" + File.separator + "deadHuman.gif";
	
	public HumanView(Human creature, WorldView worldView) {
		super(creature, worldView, urlImageAliveHuman, urlImageDeadHuman);
	}
}
