/**
 * HEIG-VD
 * MCR - Projet "Pandemie"
 * Auteurs: Thierry Forchelet, Louis Jaquier, Grégory Moinat, Julien Rebetez
 */

package gui;

import java.io.File;

import visitable.Chicken;

/**
 * Représentation graphique d'un poulet
 */
public class ChickenView extends CreatureView {
	private final static String urlImageAliveChicken = System.getProperty("user.dir") + File.separator + "res" + File.separator + "textures" + File.separator + "aliveChicken.gif";
	private final static String urlImageDeadChicken = System.getProperty("user.dir") + File.separator + "res" + File.separator + "textures" + File.separator + "deadChicken.gif";
	
	public ChickenView(Chicken creature, WorldView worldView) {
		super(creature, worldView, urlImageAliveChicken, urlImageDeadChicken);
	}
}
